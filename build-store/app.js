var createError = require('http-errors');
var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

//routers
var indexRouter = require('./routes/index');
var categoriesRouter = require('./routes/categories');
var productsRouter = require('./routes/products');

var app = express();

//Set up default mongoose connection
const mongoDB = 'mongodb://127.0.0.1/shop';
mongoose.connect(mongoDB);
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

//static
app.use(express.static(path.join(__dirname, 'public')));
app.use('*/images',express.static('public/images'));

//
var breadcrumbs = require('express-breadcrumbs');
app.use(breadcrumbs.init());
 
// Set Breadcrumbs home information
app.use(breadcrumbs.setHome());
 
// Mount the breadcrumbs
app.use('/', breadcrumbs.setHome({
  name: 'Home',
  url: '/'
}));

global.appTitle = 'Build Store';
global.appFooter = 'Copyright 2019 OSF Global Services';
global.appBreadcrumb = [];
global.rootMenu = [];

app.use('/', indexRouter);
app.use('/categories', categoriesRouter);
app.use('/products', productsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
