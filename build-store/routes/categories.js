var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Category = require('../models/categories');


router.get('/:catid', function(req, res, next) {  
  var catid = req.params.catid;

  // get prev main category choice
  global.rootMenu.find(x => x.id !== catid).choice = false;
  global.rootMenu.find(x => x.id == catid).choice = true;
 
  var sc_tmp = [];

  req.breadcrumbs('Category', req.baseUrl + '/' + catid);
  global.appBreadcrumb = req.breadcrumbs();   
   
  // find subcategories
  Category.find({'id' : catid},  function (err, subCategories) {
    let scat =  subCategories[0].categories;
    let subflated;

    Category.find({'id' : catid}, {'categories': 'categories'},  function (err, scdetail) {

      for (var i = 0; i < scdetail[0].categories.length; i++) {
          let el = scdetail[0].categories[i].categories;
          let flated = [].concat.apply([], el);
          sc_tmp.push(flated);
      }

      subflated = [].concat.apply([], sc_tmp);

      res.render('categories', { 
          title: global.appTitle,
          breadcrumbs: global.appBreadcrumb,
          menuitems: global.rootMenu,
          footer: global.appFooter,
          pagetitle: subCategories[0].page_title,
          pagedesc: subCategories[0].page_description,
          subitems: subflated
      });

    });

  });
        
    
});  

module.exports = router;  