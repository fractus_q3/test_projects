var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Category = require('../models/categories');


router.get('/', function(req, res, next) {
  global.appBreadcrumb  = req.breadcrumbs();
  var tmp = [];

  Category.find({'parent_category_id':'root'},{'id':'id', 'name':'name'},  function (err, mainCategories) {  
    //mainCategories = ['Mens', 'Womens'];
    // attach choice option to control selected main category
    for (var i = 0; i < mainCategories.length; i++) {
      let el = mainCategories[i];
      let ob = {};
      ob.id = el.id;
      ob.name = el.name;
      ob.choice = false;
      tmp.push(ob);
    }; 

    global.rootMenu = tmp;

    res.render('index', { 
      title: global.appTitle,
      breadcrumbs: global.appBreadcrumb,
      menuitems: global.rootMenu,
      footer: global.appFooter  });
  });

});

module.exports = router;
