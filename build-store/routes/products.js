var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Category = require('../models/categories');
var Product = require('../models/products');


router.get('/:catid', function(req, res, next) {
  var catid = req.params.catid;

  var pro = {'name':'Products', 'url':req.baseUrl + '/' + catid }
  if (global.appBreadcrumb && global.appBreadcrumb[global.appBreadcrumb.length-1].name !== pro.name) {
    global.appBreadcrumb.push(pro);
  };

  var sc_tmp = [];
  var lastId = null;   

  Product.aggregate([{ '$match': {'primary_category_id': catid}},
                      { '$unwind' : '$image_groups' },
                      { '$project' : { 'vt':'$image_groups.view_type',
                          'id': '$id',
                          'name': '$name',
                          'price': '$price',
                          'currency': '$currency',
                          'short_description': '$short_description',
                          'link': '$image_groups.images.link' }},
                      { '$match': {'vt': 'medium'}}],  function (err, pdlink) { 

    for (var i = 0; i < pdlink.length; i++) { 
      //create easy-use custom collection with 1st medium link
      let currId = pdlink[i]['id'];
      if (currId !== lastId) {
        lastId = currId;

        let tmp = {};
        tmp.id = pdlink[i].id;
        tmp.name = pdlink[i].name;
        
        // currency symbol format
        switch(pdlink[i].currency) {
          case 'USD':
            tmp.price = '$ '  + pdlink[i].price.toFixed(2);
            break;
          case 'EUR':
            tmp.price = '€ '  + pdlink[i].price.toFixed(2);
            break;  
          default:
            tmp.price = '$ ' + pdlink[i].price.toFixed(2);
        }

        tmp.short_description = pdlink[i].short_description;
        tmp.link = pdlink[i].link[0];

        sc_tmp.push(tmp);
      };
    };
  
    res.render('products', { 
        title: global.appTitle,
        breadcrumbs: global.appBreadcrumb,
        menuitems: global.rootMenu,
        footer: global.appFooter,
        categoryid:catid,
        products:sc_tmp
    });

  });

    
});  

module.exports = router;  