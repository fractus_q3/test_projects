var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var schema = new Schema({

  categories: {type:[]}, 
  id: {type:String, lowercase: true, trim: true, required:true},
  parent_category_id: {type:String, lowercase: true, trim: true, required:true},


  name: {type:String},
  page_description: {type:String},
  page_title: {type:String},


});

module.exports = mongoose.model('SubCategories', schema);