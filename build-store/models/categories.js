var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var subcategory = new Schema({ 
    id : {type:String},
    image : {type:String},
    name : {type:String},
    page_description : {type:String},
    page_title : {type:String},
    parent_category_id : {type:String} 
});

var schema = new Schema({
  id: {type:String, lowercase: true, trim: true, required:true},
  name : {type:String},
  page_description: {type:String},
  page_title: {type:String},
  parent_category_id : {type:String},
  c_showInMenu :{type:Boolean},
  categories: [{categories: [subcategory]}]
});

module.exports = mongoose.model('Categories', schema);