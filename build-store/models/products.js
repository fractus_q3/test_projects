var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var subImage = new Schema({ 
     images : [],
     variation_value : {type:String},
     view_type : {type:String} 
});

var schema = new Schema({
  id: {type:String, lowercase: true, trim: true, required:true},
  name : {type:String},
  page_description: {type:String},
  page_title: {type:String},
  primary_category_id : {type:String},
  image_groups: [{subImage}]
});

module.exports = mongoose.model('Products', schema);